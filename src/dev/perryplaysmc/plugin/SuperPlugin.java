package dev.perryplaysmc.plugin;

import dev.perryplaysmc.configuration.Config;
import dev.perryplaysmc.core.Core;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.sources.User;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionEffectTypeWrapper;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.plugin
 * Class: SuperPluigin
 * <p>
 * Path: dev.perryplaysmc.plugin.SuperPluigin
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class SuperPlugin extends Core {

    boolean devMode = true;

    @Override protected void onInitialize() {
        registerCommands("dev.perryplaysmc.plugin.commands");
        registerEvents("dev.perryplaysmc.plugin.listeners");
        int x = 98;
        for(x = 98; x > 10; x-=10) {
            System.out.println(x);
        }
        System.out.println(x+(10-(x%10)));
    }
}
