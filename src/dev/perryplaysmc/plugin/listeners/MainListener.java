package dev.perryplaysmc.plugin.listeners;

import dev.perryplaysmc.plugin.SuperPlugin;
import dev.perryplaysmc.utils.Utils;
import dev.perryplaysmc.utils.world.blocks.BlockHelper;
import dev.perryplaysmc.utils.inventory.ItemBuilder;
import org.bukkit.block.Block;
import org.bukkit.block.Dispenser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Directional;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.plugin.listeners
 * Class: MainListener
 * <p>
 * Path: dev.perryplaysmc.plugin.listeners.MainListener
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class MainListener implements Listener {


    @EventHandler
    void dispense(BlockDispenseEvent e) {
        if(e.getItem().getType().name().contains("PICKAXE")) {
            Block b = e.getBlock().getRelative(((Directional) e.getBlock().getState().getData()).getFacing());
            Dispenser dis = (Dispenser) e.getBlock().getState();
            ItemStack i = e.getItem();
            ItemStack f = e.getItem();
            i = Utils.ItemUtil.damage(i, 1, new Random());
            ItemStack finalI = i;
            e.setCancelled(true);
            if(ItemBuilder.isAir(b.getType())) return;
            (new BukkitRunnable() {
                @Override
                public void run() {
                    dis.getInventory().remove(f);
                    if(finalI.getDurability() <= finalI.getType().getMaxDurability()) {
                        dis.getInventory().addItem(finalI);
                    }
                    Utils.BlockHelper.breakBlockNaturally(finalI, b);
                }
            }).runTaskLater(SuperPlugin.getInstance(), 0);
            return;
        }
        if(e.getItem().getType().isBlock()) {
            Block b = e.getBlock();
            Block b1 = e.getBlock().getRelative(((Directional) e.getBlock().getState().getData()).getFacing());
            Dispenser con = (Dispenser) b.getState();
            e.setCancelled(true);
            if(!ItemBuilder.isAir(b1.getType())) return;
            b1.setType(e.getItem().getType());
            con.getInventory().remove(new ItemStack(e.getItem().getType(), 1));
            con.update(true, true);
        }
    }

    @EventHandler
    void regenExplosion(EntityExplodeEvent e) {
        BlockHelper.regenBlocks(e.blockList(), 4, 40, 1);
    }

}
