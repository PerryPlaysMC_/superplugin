package dev.perryplaysmc.plugin.commands.player.staff.moderator;

import dev.perryplaysmc.commands.Command;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.GameMode;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.plugin.commands
 * Class: CommandGamemode
 * <p>
 * Path: dev.perryplaysmc.plugin.commands.player.staff.moderator.CommandGamemode
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandGamemode extends Command {

    public CommandGamemode() {
        super("gamemode", "gamemode:gm", "gms", "gmc", "gma", "gmsp", "gms:gm0", "gmc:gm1", "gma:gm2", "gmsp:gm3");
        addPermissions(
                "gamemode:gamemode.use",
                "gms:gamemode.survival",
                "gmc:gamemode.creative",
                "gma:gamemode.adventure",
                "gmsp:gamemode.spectator"
        );
        setDescription("Change peoples gamemode or your own!");
    }

    @Override public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        String split = label.equals("gamemode") ? "gamemode" : "gm";
        args(0, () -> {
            commandType(CommandType.CONSOLE_SPECIFY_USER);
            if(label.equals("gamemode", "gm")) {
                hasPermission("gamemode.check");
                sendMessageEnd("Gamemode.current", s.getUser().getGameMode());
            }
            GameMode mode = getGameMode(new Argument(label.getName().split(split)[1]));
            hasPermission("gamemode." + mode.name().toLowerCase());
            s.getUser().setGameMode(mode);
            sendMessage("Gamemode.self", s.getUser().getGameMode());
        });
        args(1, () -> {
            if(label.equals("gamemode", "gm")) {
                commandType(CommandType.CONSOLE_SPECIFY_USER);
                hasPermission("gamemode." + getGameMode(args[0]).name().toLowerCase());
                s.getUser().setGameMode(getGameMode(args[0]));
                sendMessageEnd("Gamemode.self", s.getUser().getGameMode());
            }
            GameMode mode = getGameMode(new Argument(label.getName().split(split)[1]));
            OfflineUser t = args[0].getAnyUser();
            if(t.getSource().isOnline())
                runNormal((User)t, 0);
            hasPermission("gamemode.other." + mode.name().toLowerCase());
            t.setGameMode(mode);
            sendMessage("Gamemode.other.sender", t.getName(), t.getGameMode());
            sendMessage(t, "Gamemode.other.sendTarget", "Gamemode.other.target", s.getName(), t.getGameMode());
        });
        if(label.equals("gamemode", "gm")) {
            args(2, () -> {
                if(label.equals("gamemode", "gm")) {
                    OfflineUser t = args[0].getAnyUser();
                    if(t.getSource().isOnline())
                        runNormal((User)t, 1);
                    hasPermission("gamemode.other." + getGameMode(args[0]).name().toLowerCase());
                    t.setGameMode(getGameMode(args[0]));
                    sendMessage("Gamemode.other.sender", t.getName(), t.getGameMode());
                    sendMessageEnd(t, "Gamemode.other.sendTarget", "Gamemode.other.target", s.getName(), t.getGameMode());
                }
                sendMessageEnd("Error.tooManyArgs");
            });
        }
        sendMessage("Error.tooManyArgs");
    }
    GameMode getGameMode(Argument mode) throws CommandException {
        if(mode.equals("survival ", "s ", "0")) return GameMode.SURVIVAL;
        if(mode.equals("creative ", "c ", "1")) return GameMode.CREATIVE;
        if(mode.equals("adventure", "a ", "2")) return GameMode.ADVENTURE;
        if(mode.equals("spectator", "sp", "3")) return GameMode.SPECTATOR;
        throw new CommandException(StringUtils.Formatter.formatDefault("Messages.Error.invalidGamemode", mode.getInput()));
    }
}