package dev.perryplaysmc.plugin.commands.player.staff.moderator;

import dev.perryplaysmc.commands.Command;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.User;
import org.bukkit.Color;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.plugin.commands.player.staff.moderator
 * Class: CommandHeal
 * <p>
 * Path: dev.perryplaysmc.plugin.commands.player.staff.moderator.CommandHeal
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandHeal extends Command {

    public CommandHeal() {
        super("heal");
        setDescription("Heals the provided player or yourself");
    }

    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        args(0, () -> {
            commandType(CommandType.CONSOLE_SPECIFY_USER);
            User u = s.getUser();
            heal(u);
            sendMessage("Heal.self");
        });
        args(1, () -> {
            User t = args[0].getUser();
            runNormal(t, 0);
            heal(t);
            sendMessage("Heal.other.sender", t.getName());
            sendMessage(t,"Heal.other.sendTarget", "Heal.other.target", s.getName());
        }, new CommandException(args.length == 0 ? "Error.notEnoughtArgs" : "Error.tooManyArgs", new Object[0]));
    }

    void heal(User t) {
        t.setHealth(t.getMaxHealth());
        t.setHunger(t.getMaxHunger());
        t.setSaturation(t.getMaxSaturation());
        //
        PotionEffectType type = null;
        if((type = PotionEffectType.getByName("BAD_OMEN")) != null) t.getSource().removePotionEffect(type);
        if((type = PotionEffectType.getByName("BLINDNESS")) != null) t.getSource().removePotionEffect(type);
        if((type = PotionEffectType.getByName("CONFUSION")) != null) t.getSource().removePotionEffect(type);
        if((type = PotionEffectType.getByName("HARM")) != null) t.getSource().removePotionEffect(type);
        if((type = PotionEffectType.getByName("HUNGER")) != null) t.getSource().removePotionEffect(type);
        if((type = PotionEffectType.getByName("POISON")) != null) t.getSource().removePotionEffect(type);
        if((type = PotionEffectType.getByName("SLOW")) != null) t.getSource().removePotionEffect(type);
        if((type = PotionEffectType.getByName("UNLUCK")) != null) t.getSource().removePotionEffect(type);
        if((type = PotionEffectType.getByName("WEAKNESS")) != null) t.getSource().removePotionEffect(type);
        if((type = PotionEffectType.getByName("SLOW_DIGGING")) != null) t.getSource().removePotionEffect(type);
        if((type = PotionEffectType.getByName("WITHER")) != null) t.getSource().removePotionEffect(type);
    }
}