package dev.perryplaysmc.plugin.commands.player.staff.admin;

import dev.perryplaysmc.commands.Command;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import dev.perryplaysmc.utils.world.blocks.Block;

import java.util.HashSet;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.plugin.commands.player.staff.admin
 * Class: CommandBlockInfo
 * <p>
 * Path: dev.perryplaysmc.plugin.commands.player.staff.admin.CommandBlockInfo
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandBlockInfo extends Command {

    public CommandBlockInfo() {
        super("BlockInfo");
    }

    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        Block block = getTargetedBlock(15);
        block.getBlock().getTemperature();
        block.getBlock().getHumidity();
        sendMessage("Block.info"
                , StringUtils.getNameFromEnum(block.getBlock().getType())
                , StringUtils.getNameFromEnum(block.getBlock().getBiome())
                , block.getBlock().getLightLevel()
                , block.getBlock().getLightFromSky()
                , block.getBlock().getLightFromBlocks()
                , block.getBlock().getState().getClass().getSimpleName()
                , StringUtils.getNameFromEnum(block.getBlock().getPistonMoveReaction())
                , block.getState().isPlaced()
        );
    }

    Block getTargetedBlock(int range) throws CommandException {
        org.bukkit.block.Block b = sender.getUser().getSource().getTargetBlock(null, range);
        if(b==null)throw new CommandException("Error.invalidBlock", range);
        return new Block(b);
    }
}