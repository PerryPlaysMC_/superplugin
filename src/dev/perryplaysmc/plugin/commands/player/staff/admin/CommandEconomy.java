package dev.perryplaysmc.plugin.commands.player.staff.admin;

import dev.perryplaysmc.commands.Command;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.utils.text.Info;
import dev.perryplaysmc.utils.text.strings.NumberUtil;

import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.plugin.commands.player.staff.admin
 * Class: CommandEconomy
 * <p>
 * Path: dev.perryplaysmc.plugin.commands.player.staff.admin.CommandEconomy
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandEconomy extends Command {

    public CommandEconomy() {
        super("economy", "economy:eco");
        addPermissions("economy:eco.use");
        setMaxTabIndex(3);
        setAllowMaxTabIndex(true);
        setDescription("Allows you to edit other peoples balances");
    }

    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        args(3, () -> {
            Argument type = args[0];
            OfflineUser t = args[1].getAnyUser();
            Double money = args[2].getMoney();
            if(type.equals("set")) {
                hasPermission("eco.set");
                t.setBalance(money);
                if(t.getName().equalsIgnoreCase(s.getName())) {
                    sendMessage("Economy.Set.self", NumberUtil.format(money));
                    return;
                }
                sendMessage("Economy.Set.other.sender", t.getName(), t.getFormattedBalance(), NumberUtil.format(money));
                sendMessageOther(t, true,
                        "Economy.Set", s.getName(), t.getFormattedBalance(), NumberUtil.format(money));
                return;
            }
            if(type.equals("add", "give")) {
                hasPermission("eco.give");
                t.deposit(money);
                if(t.getName().equalsIgnoreCase(s.getName())) {
                    sendMessage("Economy.Give.self", NumberUtil.format(money), t.getFormattedBalance(), t.getBalance());
                    return;
                }
                sendMessage("Economy.Give.other.sender",
                        NumberUtil.format(money), t.getName(), t.getFormattedBalance(), t.getBalance());
                sendMessageOther(t, true,
                        "Economy.Give", s.getName(), t.getFormattedBalance(), NumberUtil.format(money));
                return;
            }
            if(type.equals("remove", "take")) {
                hasPermission("eco.take");
                t.withdraw(money);
                if(t.getName().equalsIgnoreCase(s.getName())) {
                    sendMessage("Economy.Take.self", NumberUtil.format(money), t.getFormattedBalance(), t.getBalance());
                    return;
                }
                sendMessage("Economy.Take.other.sender"
                        , t.getName(), t.getFormattedBalance(), NumberUtil.format(money), t.getBalance());
                sendMessageOther(t, true,
                        "Economy.Take", s.getName(), t.getFormattedBalance(), NumberUtil.format(money));
                return;
            }
        }, help());
    }

    CommandException help() {
        Info info = new Info(this);
        info.addSub(new String[]{"eco.set"}, "set [a]<user> [r]<money>")
                .tooltip("[c]Description: ",
                        " [pc]-[c] Sets the provided user's balance",
                        "[c]Params: ",
                        " [pc]-[a] User[c](Player)",
                        " [pc]-[a] Money[c](Number)",
                        "[c]Click to suggest: /[pc]{0} [r]set ")
                .suggest("set ");
        info.addSub(new String[]{"eco.give"}, "give [a]<user> [r]<money>")
                .tooltip("[c]Description: ",
                        " [pc]-[c] Gives money to provided user",
                        "[c]Params: ",
                        " [pc]-[a] User[c](Player)",
                        " [pc]-[a] Money[c](Number)",
                        "[c]Click to suggest: /[pc]{0} [r]give ")
                .suggest("give ");
        info.addSub(new String[]{"eco.take"}, "take [a]<user> [r]<money>")
                .tooltip("[c]Description: ",
                        " [pc]-[c] Takes money from provided user",
                        "[c]Params: ",
                        " [pc]-[a] User[c](Player)",
                        " [pc]-[a] Money[c](Number)",
                        "[c]Click to suggest: /[pc]{0} [r]take ")
                .suggest("take ").send(sender);
        return new CommandException();
    }

    @Override
    public List<String> onTab(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        List<String> f = newList();
        f.addAll(loadTab(1, list("give", "take", "set")));
        f.addAll(loadTab(2));
        f.addAll(loadTab(3, list("0", "1", "10", "100", "1000", "10000", "100000", "1000000", "10000000")));
        return f;
    }
}