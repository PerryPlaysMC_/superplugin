package dev.perryplaysmc.plugin.commands.player.staff.admin;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import dev.perryplaysmc.commands.Command;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.utils.text.strings.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.plugin.commands.player.staff.admin
 * Class: CommandOperator
 * <p>
 * Path: dev.perryplaysmc.plugin.commands.player.staff.admin.CommandOperator
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandOperator extends Command {

    public CommandOperator() {
        super("operator", "operator:op", "deoperate", "deoperate:deop");
        addPermissions(
                "operator:operator.use",
                "deoperate:deoperate.use"
        );
        setDescription("Op/Deop's people");
    }

    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        if(label.equals("op", "operator"))
            argsGreater(0, () -> {
                List<String> oppedUsers = newList(), invalidUsers = newList(), nothingChanged = newList(), loaded = newList();
                for(Argument arg : args) {
                    OfflineUser t;
                    if(loaded.contains(arg.getInput())) continue;
                    else loaded.add(arg.getInput());
                    try {
                        t = arg.getAnyUser();
                    }catch (CommandException e) {
                        if(!invalidUsers.contains(arg.getInput()))
                            invalidUsers.add(arg.getInput());
                        continue;
                    }
                    if(t.isOpped()) {
                        if(!nothingChanged.contains(t.getName()))
                            nothingChanged.add(t.getName());
                    }else {
                        t.setOp(true);
                        if(!oppedUsers.contains(t.getName()))
                            oppedUsers.add(t.getName());
                    }
                }
                if(oppedUsers.size()>0)
                    sendMessage("Operator.op", StringUtils.join(StringUtils.convertToArray(oppedUsers), "[c], [pc]", 0));
                if(invalidUsers.size()>0)
                    sendMessage("Operator.invalidUsers", StringUtils.join(StringUtils.convertToArray(invalidUsers), "[c], [pc]", 0));
                if(nothingChanged.size()>0)
                    sendMessage("Operator.nothingChanged", StringUtils.join(StringUtils.convertToArray(nothingChanged), "[c], [pc]", 0));
            });
        if(label.equals("deop", "deoperate"))
            argsGreater(0, () -> {
                List<String> deOppedUsers = newList(), invalidUsers = newList(), nothingChanged = newList(), loaded = newList();
                for(Argument arg : args) {
                    OfflineUser t;
                    if(loaded.contains(arg.getInput())) continue;
                    else loaded.add(arg.getInput());
                    try {
                        t = arg.getAnyUser();
                    }catch (CommandException e) {
                        if(!invalidUsers.contains(arg.getInput()))
                            invalidUsers.add(arg.getInput());
                        continue;
                    }
                    if(!t.isOpped()) {
                        if(!nothingChanged.contains(t.getName()))
                            nothingChanged.add(t.getName());
                    }else {
                        t.setOp(false);
                        if(!deOppedUsers.contains(t.getName()))
                            deOppedUsers.add(t.getName());
                    }
                }
                if(deOppedUsers.size()>0)
                    sendMessage("Operator.deop", StringUtils.join(StringUtils.convertToArray(deOppedUsers), "[c], [pc]", 0));
                if(invalidUsers.size()>0)
                    sendMessage("Operator.invalidUsers", StringUtils.join(StringUtils.convertToArray(invalidUsers), "[c], [pc]", 0));
                if(nothingChanged.size()>0)
                    sendMessage("Operator.nothingChanged", StringUtils.join(StringUtils.convertToArray(nothingChanged),
                            "[c], [pc]", 0));
            });
    }

    @Override
    public List<String> onTab(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        List<String> f = newList();
        List<String> l = Lists.newArrayList();
        if(args.length == 0) {
            return ImmutableList.of();
        }
        String name = args[args.length+-1].getInput();
        boolean isOp = label.equals("deop", "deoperate");
        for(User u : SuperAPI.getOnlineUsers()) {
            if(u.isOpped() == isOp)
                if(!l.contains(u.getName()) && !isArg(u.getName()))
                    l.add(u.getName());
        }
        for(OfflineUser u : SuperAPI.getOfflineUsers()) {
            if(u.isOpped() == isOp)
                if(!l.contains(u.getName()) && !isArg(u.getName()))
                    l.add(u.getName());
        }
        for(String u : l) {
            if(u.toLowerCase().startsWith(name.toLowerCase()) && !isArg(u)) f.add(u);
        }
        return f;
    }
}