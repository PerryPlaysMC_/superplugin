package dev.perryplaysmc.plugin.commands.player.builder;

import dev.perryplaysmc.commands.Command;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.utils.TimerUtility;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.plugin.commands.player.builder
 * Class: CommandBuilderMode
 * <p>
 * Path: dev.perryplaysmc.plugin.commands.player.builder.CommandBuilderMode
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandBuilderMode extends Command {

    ItemStack wand;

    public CommandBuilderMode() {
        super("buildermode",
                "buildermode:bm",
                "buildermode:builderm",
                "buildermode:bmode");
        setDescription("Puts you into BuilderMode");
    }

    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        if(s.getUser().getCurrentInventory().equalsIgnoreCase("default"))
            s.getUser().addInventory("default", s.getUser().getInventory());
        else {
            s.getUser().swapInventory("default");
            return;
        }
        s.getUser().addInventory("test", Bukkit.createInventory(null, InventoryType.CHEST));
        s.getUser().swapInventory("test");
    }
}