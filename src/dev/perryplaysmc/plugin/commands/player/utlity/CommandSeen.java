package dev.perryplaysmc.plugin.commands.player.utlity;

import dev.perryplaysmc.commands.Command;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.utils.text.strings.TimeUtil;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.plugin.commands.player
 * Class: CommandSeen
 * <p>
 * Path: dev.perryplaysmc.plugin.commands.player.utlity.CommandSeen
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandSeen extends Command {

    public CommandSeen() {
        super("Seen");
        setDescription("Tells you when someone was last seen or how long they've been on");
    }

    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        args(1, () -> {
            OfflineUser t = args[0].getAnyUser();
            if(t.getSource().isOnline())
                sendMessage("Seen.online", t.getName(), TimeUtil.formatDateDiffReverse(t.getJoinTime()));
            else
                sendMessage("Seen.offline", t.getName(), TimeUtil.formatDateDiffReverse(t.getQuitTime()));
        }, new CommandException(args.length == 0 ? "Error.notEnoughtArgs" : "Error.tooManyArgs", new Object[0]));
    }
}