package dev.perryplaysmc.plugin.commands.player.utlity;

import com.mysql.fabric.xmlrpc.base.Array;
import dev.perryplaysmc.commands.Command;
import dev.perryplaysmc.commands.CommandExecutor;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.utils.text.InteractableList;

import java.util.Arrays;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.plugin.commands.player.utlity
 * Class: CommandMain
 * <p>
 * Path: dev.perryplaysmc.plugin.commands.player.utlity.CommandMain
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandMain extends Command {

    public CommandMain() {
        super(SuperAPI.getSettings().getString("Data.Command.name"),
                SuperAPI.getSettings().getStringArray("Data.Command.aliases"));
        setDescription("Shows the help menu for this plugin");
    }

    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        InteractableList<Command> list = new InteractableList((label.getName() + " "),
                args.length == 0 ? 1 : args[0].getInt(),
                "[c]<&l&m----------------------------&r[c]>", CommandExecutor.getCommands(),
                 "[c]<&l&m----------------------------&r[c]>");
        list.text((cmd) -> {
            return "[c]/[pc]" + cmd.getInfo().getUsage().replace("/", "");
        });
        list.hover((cmd) -> {
            return "[c]" + cmd.getInfo().getDescription();
        });
        list.click((cmd) -> {
            return "/" + cmd.getName() + " ";
        });
        list.send(s);
    }
}