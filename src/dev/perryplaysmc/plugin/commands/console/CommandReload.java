package dev.perryplaysmc.plugin.commands.console;

import dev.perryplaysmc.commands.Command;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.Bukkit;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.plugin.commands.console
 * Class: CommandReload
 * <p>
 * Path: dev.perryplaysmc.plugin.commands.console.CommandReload
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandReload extends Command {

    public CommandReload() {
        super("rl", "rl:reload");
        setDescription("Reloads the server");

    }

    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        commandType(CommandType.CONSOLE);
        args(0,()->{
            broadcastPermission("reload.see", "Reload.warn");
            Bukkit.reload();
            broadcastPermission("reload.see", "Reload.complete");
        });
        sendArgs(true);
    }
}